

FS.HTTP.setHeadersForGet([
    ['Cache-Control', 'public, max-age=31536000']
]);

var masterStore = new FS.Store.GridFS("master", {})

var thumbnailStore = new FS.Store.GridFS("thumbnail", {});


ImagesStore = new FS.Collection('images-store', {
 stores: [masterStore, thumbnailStore],
 filter: {
   allow: {
     contentTypes: ['image/*'] //allow only images in this FS.Collection
   }
 }
});

Images = new Mongo.Collection('images')


if (Meteor.isServer) {



Meteor.methods({
  cropImage: function (imageId, cropInfo) {
      // check(imageId, String);
      // throw new Meteor.Error("404", "Temporarily Unavailable");



          var oldId = imageId;

          console.log('called');

          //get file and append crop info
          var fileObj = ImagesStore.findOne({_id: imageId});

          //override cropinfo if provided
          if ( cropInfo ) {
              fileObj.cropInfo = cropInfo;
          }

          //we may already have it and it won't be explicitly set
          if ( !fileObj.cropInfo ) {
              return;
          }

          console.log("OUTSIDE ++ ", cropInfo);

          //modify the stream in place
          // var readStream = fileObj.createReadStream("master");
          // var writeStream = fileObj.createWriteStream("thumbnail");
          // console.log("OUTSIDE ++ readStream ", readStream);
          // console.log("OUTSIDE ++  writeStream ", writeStream);
          Meteor._sleepForMs(2000);
          // gm(readStream, fileObj.name)
          //   // .crop(cropInfo.width, cropInfo.height, cropInfo.x, cropInfo.y )
          //   // .resize(600, 400, "^" )
          //   .stream()
          //   .pipe(writeStream);

          // Meteor._sleepForMs(2000);

          console.log('cropped');

          //update record
          return ImagesStore.update({_id: fileObj._id}, {$set: {cropInfo: cropInfo}});

  },
});


}
