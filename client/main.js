import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';

import './main.html';


var b64ToBlob = function(b64Data, contentType, sliceSize) {
    var byteArray, byteArrays, byteCharacters, byteNumbers, i, offset, slice
    sliceSize = sliceSize || 512
    byteCharacters = atob(b64Data)
    byteArrays = []
    offset = 0
    while (offset < byteCharacters.length) {
        slice = byteCharacters.slice(offset, offset + sliceSize)
        byteNumbers = (function() {
            var j, ref, results
            results = []
            for (i = j = 0, ref = slice.length; 0 <= ref ? j < ref : j > ref; i = 0 <= ref ? ++j : --j) {
                results.push(slice.charCodeAt(i))
            }
            return results
        })()
        byteArray = new Uint8Array(byteNumbers)
        byteArrays.push(byteArray)
        offset += sliceSize
    }
    return new Blob(byteArrays, { type: contentType })
}

// update info by cropping (onChange and onSelect events handler)
var updateInfo = function updateInfo(e) {
    if(parseInt(e.w) > 0) {
        var imageObj = $("#jcrop-preview")[0]
        var canvas = $("#jcrop-canvas")[0]
        // TODO: this controls the size of the canvas eventually sent to S3
        // it's 300 because it's @2x size of canvas element on page for retina support
        canvas.width = 600
        canvas.height = 400
        var context = canvas.getContext("2d")
        context.drawImage(imageObj, e.x, e.y, e.w, e.h, 0, 0, canvas.width, canvas.height)
    }
}



Template.hello.onCreated(function helloOnCreated() {
  // counter starts at 0
  this.counter = new ReactiveVar(0);
  this.isFileSelected = new ReactiveVar( false );
  // this.isUploading = new ReactiveVar( false );
  this.isFileUpdate = new ReactiveVar( false );
  this.fileName = new ReactiveVar( 0 );

});

Template.hello.helpers({
  counter() {
    return Images.find().count();
  },
  fileStatus() {
      return Template.instance().isFileSelected.get();
  },
  fileUpdate() {
      return Template.instance().isFileUpdate.get();
  },
  images() {
    return ImagesStore.find();
  },
});

Template.hello.events({
  //select and edit view
  'change #jcrop-file' (event, template) {

      // get selected file
      var oFile = $('#jcrop-file')[0].files[0]
      console.log(oFile);
      // hide all errors
      $('#jcrop-error').hide()

      // check for image type (jpg and png are allowed)
      var rFilter = /^(image\/jpeg|image\/png)$/i
      if (! rFilter.test(oFile.type)) {
          $('#jcrop-error').html('Please select a valid image file (jpg and png are allowed)').show()
          return
      }

      // check for file size
      //if (oFile.size > 500 * 1024) {
      //    $('#jcrop-error').html('The file you selected is too big, please select a file under 500Kb').show()
      //    return
      //}

      // preview element
      var oImage = document.getElementById('jcrop-preview')

      template.isFileSelected.set(true);

      // prepare HTML5 FileReader
      var oReader = new FileReader()
      oReader.onload = function(e) {

          // e.target.result contains the DataURL which we can use as a source of the image
          oImage.src = e.target.result
          oImage.onload = function () { // onload event handler

              // display step 2
              $('#jcrop-step2').fadeIn(500)

              // destroy Jcrop if it is existed
              if (typeof jcrop_api != 'undefined') {
                  //jcrop_api.destroy()
                  //jcrop_api = null
                  //$('#edit-photo-preview').width(oImage.naturalWidth)
                  //$('#edit-photo-preview').height(oImage.naturalHeight)
                  //$('.jcrop-holder').remove()
                  jcrop_api.setImage(oImage.src)
              }

              $('#jcrop-preview').Jcrop({
                  minSize: [200, 200], // min crop size
                  aspectRatio : 1, // keep aspect ratio 1:1
                  bgFade: true, // use fade effect
                  bgOpacity: .5, // fade opacity
                  boxWidth: 300,
                  boxHeight: 0,
                  setSelect:  [ 100, 50, 400, 350 ],
                  onChange: updateInfo,
                  onSelect: updateInfo
              }, function(){

                  // use the Jcrop API to get the real image size
                  var bounds = this.getBounds()
                  boundx = bounds[0]
                  boundy = bounds[1]

                  // Store the Jcrop API in the jcrop_api variable
                  jcrop_api = this
              })

          }
      }
      // read selected file as DataURL
      oReader.readAsDataURL(oFile)
  },
  //crop and upload
  'submit #jcrop-form' (event, template) {
      event.preventDefault()

      template.isFileSelected.set(false)

      var canvas = $('#jcrop-canvas')[0]
      var data = canvas.toDataURL()
      var match = /^data:([^;]+);base64,(.+)$/.exec(data)
      var type = match[1]
      var b64 = match[2]
      var blob = b64ToBlob(b64, type)

      // TODO: generate your filename
      blob.name = Math.random().toString(36).substr(2, 19);


      //uploading

      // FS.Utility.eachFile(event, function(file) {
        ImagesStore.insert(blob, function (err, fileObj) {
          if (err){
             // handle error
          } else {

            var imagesURL = "/cfs/files/images/" + fileObj._id;
            var imagesId = fileObj._id;
            console.log(fileObj);
            Images.insert({URL:imagesURL, imageId:imagesId})
            template.isFileSelected.set(false);
          }
        });
      // });


  },
  //manage and edit view
  'click .manageThumb': function(event, template) {
      event.preventDefault();

      // get selected file
      let imageId = '#' + this._id;

      //file name for update
      template.fileName.set(this._id);

      let imageSrc = $(imageId).prop('src')

      // hide all errors
      $('#jcrop-error').hide()


      // preview element
      var oImage = document.getElementById('jcrop-preview')

      template.isFileUpdate.set(true);

      // prepare HTML5 FileReader

      oImage.src = imageSrc;
      oImage.onload = function () { // onload event handler

          // display step 2
          $('#jcrop-step2').fadeIn(500)

          // destroy Jcrop if it is existed
          if (typeof jcrop_api != 'undefined') {
              //jcrop_api.destroy()
              //jcrop_api = null
              //$('#edit-photo-preview').width(oImage.naturalWidth)
              //$('#edit-photo-preview').height(oImage.naturalHeight)
              //$('.jcrop-holder').remove()
              jcrop_api.setImage(oImage.src)
          }

          $('#jcrop-preview').Jcrop({
              minSize: [200, 200], // min crop size
              aspectRatio : 1, // keep aspect ratio 1:1
              bgFade: true, // use fade effect
              bgOpacity: .5, // fade opacity
              boxWidth: 300,
              boxHeight: 0,
              setSelect:  [ 100, 50, 400, 350 ],
              onChange: updateInfo,
              onSelect: updateInfo
          }, function(){

              // use the Jcrop API to get the real image size
              var bounds = this.getBounds()
              boundx = bounds[0]
              boundy = bounds[1]

              // Store the Jcrop API in the jcrop_api variable
              jcrop_api = this
          })

      }

  },
  //update selected file 
  'click #updateFile' (event, template) {
      event.preventDefault()

      template.isFileUpdate.set(false);
      var canvas = $('#jcrop-canvas')[0]
      var data = canvas.toDataURL()
      var match = /^data:([^;]+);base64,(.+)$/.exec(data)
      var type = match[1]
      var b64 = match[2]
      var blob = b64ToBlob(b64, type)

      // TODO: generate your filename



      //uploading

      // FS.Utility.eachFile(event, function(file) {
        blob._id = template.fileName.get();
        blob.name = template.fileName.get();
        ImagesStore.remove({_id:template.fileName.get()});

        ImagesStore.insert(blob, function (err, fileObj) {
          if (err){
             // handle error
          } else {

            var imagesURL = "/cfs/files/images/" + fileObj._id;
            var imagesId = fileObj._id;
            console.log(fileObj);
            Images.insert({URL:imagesURL, imageId:imagesId})

          }
        });
      // });


  },

})
